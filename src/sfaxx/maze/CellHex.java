package sfaxx.maze;

import java.util.ArrayList;
import java.util.Arrays;

public class CellHex
{
	private int x;
	private int y;
	private ArrayList<Connection> connections;
	private boolean visited;
	private Pass pass;

	public CellHex(int x, int y)
	{
		this.x = x;
		this.y = y;
		this.connections = new ArrayList<Connection>(Arrays.asList(Connection.values()));
		this.visited = false;
		this.pass = Pass.REGULAR;
	}

	public void setConnections(ArrayList<Connection> connections)
	{
		this.connections = connections;
	}

	public void setVisited()
	{
		this.visited = true;
	}

	public void setPass(Pass pass)
	{
		this.pass = pass;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public ArrayList<Connection> getConnections()
	{
		return connections;
	}

	public Pass getPass()
	{
		return this.pass;
	}

//	public Image getImage()
//	{
//		return connections == Type.WALL ? ResourceHelper.Images.BACKGROUND : pass == Pass.REGULAR ? ResourceHelper.Images.CELL : pass == Pass.ENTER ? ResourceHelper.Images.CELL_ENTER : pass == Pass.EXIT ? ResourceHelper.Images.CELL_EXIT : ResourceHelper.Images.CELL_PASS;
//	}

	public boolean isVisited()
	{
		return this.visited ? true : false;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof CellHex)
		{
			if(this.x == ((CellHex)obj).getX() && this.y == ((CellHex)obj).getY())
			{
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}

	@Override
	public String toString()
	{
		return String.format("%s [%d, %d] -  %s", visited ? "\u25cf" : "\u25cb", this.x, this.y, connections.toString());
	}
}
