package sfaxx.maze;

import com.sun.istack.internal.NotNull;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Screen extends JPanel implements Runnable
{
	private enum State
	{
		MENU, SET_DIM, MAZE, EXIT;
	}

	List<Integer> keyPressed;

	Thread thread;

	int mouseX;
	int mouseY;

	int fps = 0;
	int ups = 0;

	Maze maze;
	MazeHex hexMaze;
	private JFrame frame;
	private State state;
	private boolean isHex;

	private int setDimX;
	private int setDimY;

	private StringButton[] menuButtons;
	private StringButton returnButton;

	public Screen(JFrame frame)
	{
		//        keyPressed = new ArrayList<Integer>();
		isHex = false;
		menuButtons = new StringButton[] { new StringButton("Classic"), new StringButton("Hexagonal"), new StringButton("Exit") };
		returnButton = new StringButton("Menu", new Color(40, 196, 200), new Color(36, 200, 116));

		this.frame = frame;
		maze = new Maze(60, 60);
		hexMaze = new MazeHex(/*8, 8*/4, 4);
		state = State.MENU;

		frame.addKeyListener(new KeyListener()
		{

			@Override public void keyTyped(KeyEvent e)
			{

			}

			@Override public void keyReleased(KeyEvent e)
			{
				//if(keyPressed.contains(e.getKeyCode()))
				//                keyPressed.remove((Integer) e.getKeyCode());
			}

			@Override public void keyPressed(KeyEvent e)
			{
				if (KeyEvent.VK_F12 == e.getKeyCode())
				{
					state = State.EXIT;
					frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
				}
				else if (KeyEvent.VK_ESCAPE == e.getKeyCode())
				{
					state = State.MENU;
				}

				//                if(!keyPressed.contains(e.getKeyCode())) keyPressed.add(e.getKeyCode());
			}
		});

		frame.addMouseListener(new MouseListener()
		{

			@Override public void mouseReleased(MouseEvent e)
			{

			}

			@Override public void mousePressed(MouseEvent e)
			{
				//                if()
			}

			@Override public void mouseExited(MouseEvent e)
			{

			}

			@Override public void mouseEntered(MouseEvent e)
			{

			}

			@Override public void mouseClicked(MouseEvent e)
			{
				if (State.MENU == state)
				{
					if (menuButtons[0].isInBounds(mouseX, mouseY))
					{
						state = State.MAZE;
						isHex = false;
					}
					else if (menuButtons[1].isInBounds(mouseX, mouseY))
					{
						state = State.MAZE;
						isHex = true;
						hexMaze.clearMaze();
						hexMaze.generateMaze();
					}
					else if (menuButtons[2].isInBounds(mouseX, mouseY))
					{
						state = State.EXIT;
					}
				}
				else if(State.MAZE == state)
				{
					if(returnButton.isInBounds(mouseX, mouseY))
					{
						state = State.MENU;
					}
				}
			}
		});

		frame.addMouseMotionListener(new MouseMotionListener()
		{

			@Override public void mouseMoved(MouseEvent e)
			{
				mouseX = e.getX();
				mouseY = e.getY();
			}

			@Override public void mouseDragged(MouseEvent e)
			{
				mouseX = e.getX();
				mouseY = e.getY();
			}
		});

		thread = new Thread(this);
		thread.start();

	}

	private void update()
	{
		if (State.EXIT == state)
		{
			frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
		}
	}

	public void paintComponent(Graphics g)
	{
		String fontName = "Comic Sans MS";
		g.clearRect(0, 0, MazeGeneration.screenWidth, MazeGeneration.screenHeight);

		if (State.MAZE == state)
		{
			if(isHex)
			{
				hexMaze.screenMaze(g);

				g.setFont(new Font(fontName, Font.PLAIN, 16));
				FontMetrics fm = g.getFontMetrics();

				int x = MazeGeneration.screenWidth - 16 - fm.stringWidth(returnButton.getLabel());
				int y = MazeGeneration.screenHeight - 16;

				returnButton.x = x;
				returnButton.y = y - fm.getHeight();
				returnButton.width = fm.stringWidth(returnButton.getLabel());
				returnButton.height = y - returnButton.y;

				g.setColor(returnButton.isInBounds(mouseX, mouseY) ? returnButton.getHighlightedColor() : returnButton.getColor());
				g.drawString(returnButton.getLabel(), x, y);

				String time = String.format("%d ms", hexMaze.getGenTime());
				x = MazeGeneration.screenWidth - 16 - fm.stringWidth(time);
				y -= fm.getHeight();

				g.setColor(returnButton.getColor());
				g.drawString(time, x, y);
			}
			else
			{
				maze.screenMaze(g);

				g.setFont(new Font(fontName, Font.PLAIN, 16));
				FontMetrics fm = g.getFontMetrics();

				int x = MazeGeneration.screenWidth - 16 - fm.stringWidth(returnButton.getLabel());
				int y = MazeGeneration.screenHeight - 16;

				returnButton.x = x;
				returnButton.y = y - fm.getHeight();
				returnButton.width = fm.stringWidth(returnButton.getLabel());
				returnButton.height = y - returnButton.y;

				g.setColor(returnButton.isInBounds(mouseX, mouseY) ? returnButton.getHighlightedColor() : returnButton.getColor());
				g.drawString(returnButton.getLabel(), x, y);

				String time = String.format("%d ms", maze.getGenTime());
				x = MazeGeneration.screenWidth - 16 - fm.stringWidth(time);
				y -= fm.getHeight();

				g.setColor(returnButton.getColor());
				g.drawString(time, x, y);
			}
		}
		else if (State.MENU == state)
		{
			g.setColor(new Color(255, 230, 160));
			g.drawRect(0, 0, MazeGeneration.screenWidth - 1, MazeGeneration.screenHeight - 1);

			g.setFont(new Font(fontName, Font.BOLD, 96));
			FontMetrics fm = g.getFontMetrics();

			g.setColor(new Color(0, 128, 0));
			int y = (int) (fm.getHeight() * 1.5f);
			g.drawString("MAZE", ((MazeGeneration.screenWidth / 2) - (fm.stringWidth("MAZE") / 2)), y);

			g.setFont(new Font(fontName, Font.PLAIN, 24));
			fm = g.getFontMetrics();


			int x;
//			y = (int) (y * 1.5f);
			y += fm.getHeight() * 2;
			g.setColor(new Color(180, 200, 220));
			int w = fm.stringWidth(" 000 ");
			String xDim = String.format("%d", 160);

			g.drawRoundRect((MazeGeneration.screenWidth / 2) - (fm.stringWidth(menuButtons[0].getLabel()) / 2), y, w, fm.getHeight(), 5, 5);
			g.drawString(xDim, (MazeGeneration.screenWidth / 2) - (fm.stringWidth(menuButtons[0].getLabel()) / 2) + (w / 2 - fm.stringWidth(xDim) / 2), y + fm.getHeight() * 3 / 4/* - fm.getHeight() / 3*/);

			for(StringButton btn : menuButtons)
			{
				x = (MazeGeneration.screenWidth / 2) - (fm.stringWidth(btn.getLabel()) / 2);
				y += fm.getHeight() * 2;

				btn.x = x;
				btn.y = y - fm.getHeight();
				//				btn.y = y;
				btn.width = fm.stringWidth(btn.getLabel());
				btn.height = /*fm.getMaxAscent() + fm.getMaxDescent()*/y - btn.y;

				g.setColor(mouseX > x && mouseX < x + fm.stringWidth(btn.getLabel()) && mouseY < y && mouseY > y - fm.getHeight() ? btn.getHighlightedColor() : btn.getColor());
				g.drawString(btn.getLabel(), x, y);
			}
		}
	}

	@Override public void run()
	{
		int UPS = 20;
		int FPS = 20;

		long gameSkipTicks = 1000 / UPS;
		long frameSkipTicks = 1000 / FPS;
		long maxFrameskips = 5;

		long nextGameTick = System.currentTimeMillis();
		long nextFrameTick = System.currentTimeMillis();
		long time = System.currentTimeMillis();

		int loops;

		int frames = 0;
		int updates = 0;

		while (true)
		{
			loops = 0;
			while (System.currentTimeMillis() > nextGameTick && loops < maxFrameskips)
			{
				update();
				nextGameTick += gameSkipTicks;
				updates++;
				loops++;
			}

			if (System.currentTimeMillis() > nextFrameTick)
			{
				nextFrameTick += frameSkipTicks;
				repaint();
				frames++;
			}

			if (time + 1000 <= System.currentTimeMillis())
			{
				time += 1000;
				fps = frames;
				ups = updates;
				frames = 0;
				updates = 0;
			}

		}
	}

	private class StringButton
	{
		public int x, y, width, height;
		private String label;
		private Color color;
		private Color highlighted;

		public StringButton(String label, Color color, Color highlighted)
		{
			this.label = label;
			this.color = color;
			this.highlighted = highlighted;
		}

		public StringButton(String label)
		{
			this(label, new Color(180, 200, 220), new Color(12, 122, 225));
		}

		public String getLabel()
		{
			return label;
		}

		public Color getColor()
		{
			return color;
		}

		public Color getHighlightedColor()
		{
			return highlighted;
		}

		public boolean isInBounds(int x, int y)
		{
			return x > this.x && x < this.x + width && y > this.y && y < this.y + height;
		}
	}

	private class NumBox
	{
		public int x, y, width, height;
		private int number;
		private Color color;
		private Color inEdit;
		public boolean isEditing;

		public NumBox(int number, @NotNull Color color, @NotNull Color inEdit)
		{
			this.number = number;
			this.color = color;
			this.inEdit = inEdit;
		}

		public NumBox(int number)
		{
			this(number, new Color(180, 200, 220), new Color(12, 122, 225));
		}

		public Color getColor()
		{
			return color;
		}

		public Color getInEditColor()
		{
			return inEdit;
		}

		public void paint(Graphics g)
		{
			FontMetrics fm = g.getFontMetrics();
			String num = String.format(" %d ", number);
			int w = fm.stringWidth(num);

			g.drawRoundRect(x, y, w, height, 5, 5);


		}
	}
}
