package sfaxx.maze;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;
import java.util.Vector;

public class Maze
{
    private long genTime;
//	private long beginTime;
//	private long endTime;
    private Random rand = new Random();
    private int width;
    private int height;
    private Cell[][] cells;
    private Cell enter;
    private Cell exit;
    private Stack<Cell> rightPass;
    private ArrayList<Cell> deadlocks = new ArrayList<Cell>();
    private ArrayList<Stack<Cell>> deadlocksPasses = new ArrayList<Stack<Cell>>();
    private boolean deadended = false;

    public Maze(int w, int h)
    {
        this.width = w;
        this.height = h;
        long begTime = System.currentTimeMillis();
        fillField();
        generateMaze();
        long endTime = System.currentTimeMillis();
        genTime =endTime - begTime;
//        this.beginTime = begTime;
//        this.endTime = endTime;
    }

    public long getGenTime()
    {
        return genTime;
    }

    public Stack<Cell> getRightPass()
    {
        return rightPass;
    }


    private void fillField()
    {
        cells = new Cell[width][height];
        for(int x = 0; x < width; ++x)
        {
            for(int y = 0; y < height; ++y)
            {
                cells[x][y] = new Cell(x, y);
                if(x % 2 == 0 && y % 2 == 0) cells[x][y].setType(Type.CELL);
            }
        }
    }

    private void generateMaze()
    {
        Cell currCell;
        Vector<Cell> neighbours = new Vector<Cell>();
        Stack<Cell> currPass = new Stack<Cell>();
        boolean returned = false;

        int beginSide = rand.nextInt(4);
        int beginSideCord = beginSide == 0 || beginSide == 3 ? 0 : beginSide == 1 ? width - 1 : height - 1;
        beginSideCord -= beginSideCord % 2 == 0 ? 0 : 1;
        int beginCord = beginSide % 2 == 0 ? rand.nextInt(width) : rand.nextInt(height);
        beginCord -= beginCord % 2 == 0 ? 0 : 1;

        int difX = 0;
        int difY = 0;
        switch(beginSide)
        {
            case 0:
                difX = 0;
                difY = 2;
                break;

            case 1:
                difX = -2;
                difY = 0;
                break;

            case 2:
                difX = 0;
                difY = -2;
                break;

            case 3:
                difX = 2;
                difY = 0;
                break;

            default:
                System.err.println("Invalid begin side");
                break;
        }

        Cell begin;
        if(beginSide % 2 == 0) begin = new Cell(beginCord, beginSideCord);
        else begin = new Cell(beginSideCord, beginCord);
        exit = begin;

        cells[begin.getX()][begin.getY()].setPass(Pass.EXIT);
        cells[begin.getX()][begin.getY()].setVisited();
        currPass.push(cells[begin.getX()][begin.getY()]);
        currCell = cells[begin.getX() + difX][begin.getY() + difY];
        mergeCells(begin, currCell);

        while(!returned)
        {
            neighbours = getNeighbours(currCell, cells);
            Cell neighbour;
            int size = neighbours.size();

            if(size == 0)
            {
                if(!currPass.isEmpty())
                {
                    if(!deadended)
                    {
                        deadlocks.add(currCell);
                        deadlocksPasses.add((Stack<Cell>) currPass.clone());
                        deadended = true;
                    }
                    currCell.setVisited();
                    currCell = currPass.pop();
                }

            }
            else
            {
                neighbour = neighbours.get(rand.nextInt(size));
                mergeCells(currCell, neighbour);
                currPass.push(currCell);
                currCell.setVisited();
                currCell = neighbour;
                deadended = false;
            }

            if(currPass.isEmpty() && currCell.equals(begin)) returned = true;
        }

        int start = rand.nextInt(deadlocks.size());
        enter = deadlocks.get(start);
        enter.setPass(Pass.ENTER);
        rightPass = deadlocksPasses.get(start);

        for(Cell c : rightPass)
        {
            if(c.getPass() == Pass.REGULAR)
            {
                c.setPass(Pass.RIGHT);
            }
        }
    }

    private void generateHexMaze()
    {
        Cell currCell;
        Vector<Cell> neighbours = new Vector<Cell>();
        Stack<Cell> currPass = new Stack<Cell>();
        boolean returned = false;

        int beginSide = rand.nextInt(4);
        int beginSideCord = beginSide == 0 || beginSide == 3 ? 0 : beginSide == 1 ? width - 1 : height - 1;
        beginSideCord -= beginSideCord % 2 == 0 ? 0 : 1;
        int beginCord = beginSide % 2 == 0 ? rand.nextInt(width) : rand.nextInt(height);
        beginCord -= beginCord % 2 == 0 ? 0 : 1;

        int difX = 0;
        int difY = 0;
        switch(beginSide)
        {
        case 0:
            difX = 0;
            difY = 2;
            break;

        case 1:
            difX = -2;
            difY = 0;
            break;

        case 2:
            difX = 0;
            difY = -2;
            break;

        case 3:
            difX = 2;
            difY = 0;
            break;

        default:
            System.err.println("Invalid begin side");
            break;
        }

        Cell begin;
        if(beginSide % 2 == 0) begin = new Cell(beginCord, beginSideCord);
        else begin = new Cell(beginSideCord, beginCord);
        exit = begin;

        cells[begin.getX()][begin.getY()].setPass(Pass.EXIT);
        cells[begin.getX()][begin.getY()].setVisited();
        currPass.push(cells[begin.getX()][begin.getY()]);
        currCell = cells[begin.getX() + difX][begin.getY() + difY];
        mergeCells(begin, currCell);

        while(!returned)
        {
            neighbours = getNeighbours(currCell, cells);
            Cell neighbour;
            int size = neighbours.size();

            if(size == 0)
            {
                if(!currPass.isEmpty())
                {
                    if(!deadended)
                    {
                        deadlocks.add(currCell);
                        deadlocksPasses.add((Stack<Cell>) currPass.clone());
                        deadended = true;
                    }
                    currCell.setVisited();
                    currCell = currPass.pop();
                }

            }
            else
            {
                neighbour = neighbours.get(rand.nextInt(size));
                mergeCells(currCell, neighbour);
                currPass.push(currCell);
                currCell.setVisited();
                currCell = neighbour;
                deadended = false;
            }

            if(currPass.isEmpty() && currCell.equals(begin)) returned = true;
        }

        int start = rand.nextInt(deadlocks.size());
        enter = deadlocks.get(start);
        enter.setPass(Pass.ENTER);
        rightPass = deadlocksPasses.get(start);

        for(Cell c : rightPass)
        {
            if(c.getPass() == Pass.REGULAR)
            {
                c.setPass(Pass.RIGHT);
            }
        }
    }

    private Vector<Cell> getNeighbours(Cell cell, Cell[][] maze)
    {
        Vector<Cell> neighbours = new Vector<Cell>();
        int x = cell.getX();
        int y = cell.getY();

        for(int i = 0; i < 2; ++i)
        {
            for(int j = -2; j <= 2; j += 4)
            {
                if(i == 0)
                {
                    if(x + j >= 0 && x + j < width/* - 1*/)
                    {
                        Cell neighbour = maze[x + j][y];
                        if(neighbour.getType() == Type.CELL && !neighbour.isVisited())
                            neighbours.addElement(neighbour);
                    }
                }
                else
                {
                    if(y + j >= 0 && y + j < height/* - 1*/)
                    {
                        Cell neighbour = maze[x][y + j];
                        if(neighbour.getType() == Type.CELL && !neighbour.isVisited())
                            neighbours.addElement(neighbour);
                    }
                }
            }
        }

        return neighbours;
    }

    private void mergeCells(Cell first, Cell second)
    {
        int difX = second.getX() - first.getX();
        int difY = second.getY() - first.getY();

        int tarX = difX == 0 ? first.getX() + 0 : first.getX() + (difX / Math.abs(difX));
        int tarY = difY == 0 ? first.getY() + 0 : first.getY() + (difY / Math.abs(difY));

        cells[tarX][tarY].setType(Type.CELL);
        //currPass.push(cells[tarX][tarY]);
    }

    public void printMaze()
    {
        for(int i = 0; i < width; ++i)
        {
            for(int j = 0; j < height; ++j)
            {
                System.out.println(cells[i][j].toString());
            }
        }

        System.out.println("");
        System.out.println("Enter is: " + enter.toString());
        System.out.println("Exit is: " + exit.toString());
        System.out.println("Right pass is:");

        System.out.println(rightPass.size());

        for(Cell c : rightPass)
        {
            System.out.println(c.toString());
        }
    }

    public void screenMaze(Graphics g)
    {
        int size = 32;
        int sizeX = MazeGeneration.screenWidth / 2 - width * size;
        int sizeY = MazeGeneration.screenHeight / 2 - height * size;


        BufferedImage bufImMaze = new BufferedImage(size * width, size * height, BufferedImage.TYPE_INT_RGB);
        Graphics graphMaze = bufImMaze.getGraphics();

        for(int i = 0; i < width; ++i)
        {
            int x = i * size;
            for(int j = 0; j < height; ++j)
            {
                int y = j * size;

                graphMaze.drawImage(cells[i][j].getImage(), x, y, null);
            }
        }

        Image lab = (Image) bufImMaze;

        BufferedImage bufImResult = new BufferedImage(MazeGeneration.screenWidth, MazeGeneration.screenHeight, BufferedImage.TYPE_INT_RGB);
        Graphics graphResult = bufImResult.getGraphics();

        graphResult.drawImage(ResourceHelper.Images.BACKGROUND, 0, 0, MazeGeneration.screenWidth, MazeGeneration.screenHeight, null);
        graphResult.drawImage(lab, /*Math.abs((MazeGeneration.screenWidth / 2) - (lab.getWidth(null)) / 2)*//*300*/0, 0, MazeGeneration.screenHeight, MazeGeneration.screenHeight, null);

        Image result = (Image) bufImResult;

        g.drawImage(result, 0, 0, null);
    }

}
