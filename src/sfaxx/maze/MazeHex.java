package sfaxx.maze;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;
import java.util.Vector;

public class MazeHex
{
	private long genTime;
	private Random rand = new Random();
	private int width;
	private int height;
	private CellHex[][] cells;
	private Stack<CellHex> rightPass;
	private boolean generated;
	private CellHex enter;
	private CellHex exit;
	private ArrayList<CellHex> deadlocks = new ArrayList<>();
	private ArrayList<Stack<CellHex>> deadlocksPasses = new ArrayList<>();
	private boolean deadended = false;

	public MazeHex(int w, int h)
	{
		this.width = w;
		this.height = h;
		fillField();
		generated = false;
//		generateMaze();
	}

	public boolean isGenerated()
	{
		return generated;
	}

	public long getGenTime()
	{
		return genTime;
	}

	public Stack<CellHex> getRightPass()
	{
		return rightPass;
	}

	private void fillField()
	{
		cells = new CellHex[width][height];

		for(int x = 0; x < width; ++x)
		{
			for(int y = 0; y < height; ++y)
			{
				cells[x][y] = new CellHex(x, y);
//				if(x % 2 == 0 && y % 2 == 0) cells[x][y].setType(Type.CELL);
			}
		}
	}

	public void generateMaze()
	{
		if(generated)
		{
			return;
		}

		long begTime = System.currentTimeMillis();

		CellHex currCell;
		Vector<CellHex> neighbours = new Vector<>();
		Stack<CellHex> currPass = new Stack<>();
		boolean returned = false;

		int beginSide = rand.nextInt(4);
		int beginSideCord = beginSide == 0 || beginSide == 3 ? 0 : beginSide == 1 ? width - 1 : height - 1;
		beginSideCord -= beginSideCord % 2 == 0 ? 0 : 1;
		int beginCord = beginSide % 2 == 0 ? rand.nextInt(width) : rand.nextInt(height);
		beginCord -= beginCord % 2 == 0 ? 0 : 1;

		CellHex begin;
//		if(beginSide % 2 == 0) begin = new CellHex(beginCord, beginSideCord);
		if(beginSide % 2 == 0) begin = cells[beginCord][beginSideCord];
//		else begin = new CellHex(beginSideCord, beginCord);
		else begin = cells[beginSideCord][beginCord];
		exit = begin;

		cells[begin.getX()][begin.getY()].setPass(Pass.EXIT);
		cells[begin.getX()][begin.getY()].setVisited();
		currPass.push(cells[begin.getX()][begin.getY()]);
		currCell = begin;

		while(!returned)
		{
			neighbours = getNeighbours(currCell, cells);
			CellHex neighbour;
			int size = neighbours.size();

			if(size == 0)
			{
				if(!currPass.isEmpty())
				{
					if(!deadended)
					{
						deadlocks.add(currCell);
						deadlocksPasses.add((Stack<CellHex>) currPass.clone());
						deadended = true;
					}
					currCell.setVisited();
					currCell = currPass.pop();
				}

			}
			else
			{
				neighbour = neighbours.get(rand.nextInt(size));
				mergeCells(currCell, neighbour);
				currPass.push(currCell);
				currCell.setVisited();
				currCell = neighbour;
				deadended = false;
			}

			if(currPass.isEmpty() && currCell.equals(begin)) returned = true;
		}

		int start = rand.nextInt(deadlocks.size());
		enter = deadlocks.get(start);
		enter.setPass(Pass.ENTER);
		rightPass = deadlocksPasses.get(start);

		for(CellHex c : rightPass)
		{
			if(c.getPass() == Pass.REGULAR)
			{
				c.setPass(Pass.RIGHT);
			}
		}

		long endTime = System.currentTimeMillis();
		genTime = endTime - begTime;

		generated = true;
	}

	public void clearMaze()
	{
		cells = null;
		fillField();
	}

	private Vector<CellHex> getNeighbours(CellHex cell, CellHex[][] maze)
	{
		Vector<CellHex> neighbours = new Vector<>();
		int cellX = cell.getX();
		int cellY = cell.getY();

		for(int x = -1; x < 2; ++x)
		{
			for(int y = -1; y < 2; ++y)
			{
				if(x != y)
				{
					if(inRange(cellX + x, 0, width) && inRange(cellY + y, 0, height))
					{
						CellHex neighbour = maze[cellX + x][cellY + y];

						if(!neighbour.isVisited())
						{
							neighbours.addElement(neighbour);
						}
					}
				}
			}
		}

		return neighbours;
	}

	private boolean inRange(int number, int min, int max)
	{
		return number >= min && number < max;
	}

	private void mergeCells(CellHex first, CellHex second)
	{
		Connection connection = Connection.fromOffset(first.getX(), first.getY(), second.getX(), second.getY());

		ArrayList<Connection> connectionsFirst = cells[first.getX()][first.getY()].getConnections();
		connectionsFirst.remove(connection);
		ArrayList<Connection> connectionsSecond = cells[second.getX()][second.getY()].getConnections();
		connectionsSecond.remove(connection.opposite());
		cells[first.getX()][first.getY()].setConnections(connectionsFirst);
		cells[second.getX()][second.getY()].setConnections(connectionsSecond);
	}

	public void printMaze()
	{
		for(int i = 0; i < width; ++i)
		{
			for(int j = 0; j < height; ++j)
			{
				System.out.println(cells[i][j].toString());
			}
		}

		System.out.println("");
		System.out.println("Enter is: " + enter.toString());
		System.out.println("Exit is: " + exit.toString());
		System.out.println("Right pass is:");

		System.out.println(rightPass.size());

		for(CellHex c : rightPass)
		{
			System.out.println(c.toString());
		}
	}

	public void screenMaze(Graphics g)
	{
		int size = 32;
		int sizeX = MazeGeneration.screenWidth / 2 - width * size;
		int sizeY = MazeGeneration.screenHeight / 2 - height * size;


		BufferedImage bufImMaze = new BufferedImage(size * width, size * height, BufferedImage.TYPE_INT_RGB);
		Graphics graphMaze = bufImMaze.getGraphics();

		for(int i = 0; i < width; ++i)
		{
			int x = i * size;
			for(int j = 0; j < height; ++j)
			{
				int y = j * size;

//				graphMaze.drawImage(cells[i][j].getImage(), x, y, null);
			}
		}

		Image lab = (Image) bufImMaze;

		BufferedImage bufImResult = new BufferedImage(MazeGeneration.screenWidth, MazeGeneration.screenHeight, BufferedImage.TYPE_INT_RGB);
		Graphics graphResult = bufImResult.getGraphics();

		graphResult.drawImage(ResourceHelper.Images.BACKGROUND, 0, 0, MazeGeneration.screenWidth, MazeGeneration.screenHeight, null);
		graphResult.drawImage(lab, /*Math.abs((MazeGeneration.screenWidth / 2) - (lab.getWidth(null)) / 2)*//*300*/0, 0, MazeGeneration.screenHeight, MazeGeneration.screenHeight, null);

		Image result = (Image) bufImResult;

		g.drawImage(result, 0, 0, null);
	}
}
