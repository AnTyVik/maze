package sfaxx.maze;

import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MazeGeneration 
{
	public static int screenWidth;
	public static int screenHeight;
	
	public static void main(String[] args) 
	{
		SwingUtilities.invokeLater(() -> new MazeGeneration());
	}
	
	public MazeGeneration()
	{
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		screenWidth = toolkit.getScreenSize().width;
		screenHeight = toolkit.getScreenSize().height;
		
		JFrame window = new JFrame("Maze");
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
		window.setUndecorated(true);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.add(new Screen(window));
		window.setVisible(true);
	}
}
