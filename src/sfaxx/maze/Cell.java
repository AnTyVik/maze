package sfaxx.maze;

import java.awt.Image;

public class Cell
{
	private int x;
	private int y;
	private Type type;
	private boolean visited;
	private Pass pass;
	
	public Cell(int x, int y)
	{
		this.x = x;
		this.y = y;
		this.type = Type.WALL;
		this.visited = false;
		this.pass = Pass.REGULAR;
	}
	
	public void setType(Type type)
	{
		this.type = type;
	}
	
	public void setVisited()
	{
		this.visited = true;
	}
	
	public void setPass(Pass pass)
	{
		this.pass = pass;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public Type getType()
	{		
		return type;
	}

	public Pass getPass()
	{
		return this.pass;
	}
	
	public Image getImage()
	{
		return type == Type.WALL ? ResourceHelper.Images.BACKGROUND : pass == Pass.REGULAR ? ResourceHelper.Images.CELL : pass == Pass.ENTER ? ResourceHelper.Images.CELL_ENTER : pass == Pass.EXIT ? ResourceHelper.Images.CELL_EXIT : ResourceHelper.Images.CELL_PASS;
	}
	
	public boolean isVisited()
	{
		return this.visited ? true : false;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof Cell)
		{
			if(this.x == ((Cell)obj).getX() && this.y == ((Cell)obj).getY())
			{
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
	
	@Override
	public String toString()
	{
		return "(" + this.x + ", " + this.y + ") - " + (this.type == Type.WALL ? "Wall" : "Cell");
	}
}
