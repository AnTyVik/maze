package sfaxx.maze;

import java.awt.Image;

import javax.swing.ImageIcon;

public class ResourceHelper
{
	public static class  Images
	{
		public static Image BACKGROUND = new ImageIcon("resources/MazeBackground.png").getImage();
		public static Image CELL = new ImageIcon("resources/corridor/Regular.png").getImage();
		public static Image CELL_ENTER = new ImageIcon("resources/corridor/Enter.png").getImage();
		public static Image CELL_EXIT = new ImageIcon("resources/corridor/Exit.png").getImage();
		public static Image CELL_PASS = new ImageIcon("resources/corridor/Pass1.png").getImage();
	}
}
