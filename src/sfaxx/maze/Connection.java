package sfaxx.maze;

public enum Connection
{
	NW(5, 0, -1),
	NE(4, 1, -1),
	W(3, -1, 0),
	E(2, 1, 0),
	SW(1, -1, 1),
	SE(0, 0, 1);

	private int opposite;
	private int offsetX;
	private int offsetY;

	Connection(int opposite, int offsetX, int offsetY)
	{
		this.opposite = opposite;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}

	public Connection opposite()
	{
		return values()[opposite];
	}

	/*public Connection fromOffset(int offsetX, int offsetY)
	{
		if()
	}*/

	public static Connection fromOffset(int xFirst, int yFirst, int xSecond, int ySecond)
	{
		if(xFirst > xSecond)
		{
			if(yFirst == ySecond)
			{
				return W;
			}
			else if(yFirst < ySecond)
			{
				return SW;
			}
		}
		else if(xFirst == xSecond)
		{
			if(yFirst > ySecond)
			{
				return NW;
			}
			else if(yFirst < ySecond)
			{
				return SE;
			}
		}
		else
		{
			if(yFirst > ySecond)
			{
				return NE;
			}
			else if(yFirst == ySecond)
			{
				return E;
			}
		}

		return null;
	}
}
